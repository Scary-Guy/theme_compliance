# Theme Color Compliance
This is a list of programs that do/do not conform to dark/light modes/themes.

This list is a major work in progress and was started 12023-07-14.
This list exists because I have autism, and as such bright lights/colors are mildly triggering.
Please also note as I mostly use a Linux distro that this list will be weighted toward that.
I may do separate lists for all the major operating systems eventually.

Programs that have no theme support (as far as I can tell):
  
    NUT Monitor (note, theme is applied in Linux Mint and I'm not sure why?)

Programs that have internal theme support (must be set separately from the system theme):

    Firefox
    (Ungoogled) Chromium/Chrome
    KeepassXC
    Steam - Right click menu unthemed in Linux without effort (also can't scale the font for extra frustration.)
    Audacious/Winamp
    Virtual DJ (and most other DJ apps)
    Webcord/Discord

It should be noted that there are some apps that not only defalt to a dark mode, but completely lack a light mode as well.  This is equally as bad because some users prefer a lightmode because it may be easier for them to see (especially in direct sunlight.)
